from rest_framework import serializers
from .models import Code
from collaborating_registration.serializers import UserShortSerializer


class CodeSerializer(serializers.ModelSerializer):
    users = UserShortSerializer(
        many=True,
        read_only=True,
    )
    created_by = UserShortSerializer(
        read_only=True,
    )

    class Meta:
        model = Code
        fields = ("users", "id", "friend_id", "code", "created_by")


class CodeShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Code
        fields = ("id", "friend_id")
