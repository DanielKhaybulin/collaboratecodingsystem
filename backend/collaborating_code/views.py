from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status, viewsets
from collaborating_code.models import Code

from collaborating_registration.services import JWTPermission, get_user_from_request
from collaborating_registration.models import User
from collaborating_registration.serializers import UserShortSerializer

from collaborating_code.serializers import CodeShortSerializer

from django.conf import settings
import jwt
import datetime

import logging


@permission_classes([JWTPermission])
@api_view(["GET"])
def create_new_session(req):
    date = datetime.datetime.now() - datetime.timedelta(days=7)
    Code.objects.filter(created_at__lt=date).delete()
    user = get_user_from_request(req)
    friend_id = Code.create_unique_friend_id_()
    logging.info(f'Created friend_id: {friend_id}')
    code = Code.objects.create(friend_id=friend_id, created_by=user)
    code.users.add(user)
    code.save()
    return Response({"id": code.id, "friend_id": friend_id}, status=status.HTTP_200_OK)


@permission_classes([JWTPermission])
@api_view(["GET"])
def read_code(req, friend_id):
    code = Code.objects.get(friend_id=friend_id)
    code.code = req.data["code"]
    code.save()
    return Response(code.code, status=status.HTTP_200_OK)


@permission_classes([JWTPermission])
@api_view(["POST"])
def edit_code(req, friend_id):
    user = get_user_from_request(req)
    code = Code.objects.get(friend_id=friend_id)
    code.users.add(user)
    code.save()
    return Response(code.code, status=status.HTTP_200_OK)


@permission_classes([JWTPermission])
@api_view(["GET"])
def get_code_users(request, friend_id):
    code = Code.objects.get(friend_id=friend_id)
    users = UserShortSerializer(code.users.all(), many=True)
    return Response(users.data, status=status.HTTP_200_OK)


@permission_classes([JWTPermission])
@api_view(["GET"])
def get_user_codes(request):
    user = get_user_from_request(request)
    codes = user.codes.all()
    serializer = CodeShortSerializer(codes, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)
