from django.urls import path
from collaborating_code import views

urlpatterns = [
    path("new_session/", views.create_new_session),
    path("<str:friend_id>/edit/", views.edit_code),
    path("<str:friend_id>/read/", views.read_code),
    path("<str:friend_id>/users/", views.get_code_users),
    path("codes/", views.get_user_codes),
]
