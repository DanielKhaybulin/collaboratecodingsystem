from django.db import models
from collaborating_registration.models import User
import uuid


class Code(models.Model):
    friend_id = models.CharField(max_length=5, unique=True)
    code = models.CharField(max_length=2048)
    created_by = models.ForeignKey(
        User, related_name="created_codes", on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    users = models.ManyToManyField(
        User, related_name="codes", blank=True, null=True)

    @staticmethod
    def generate_frined_key_():
        random = str(uuid.uuid4())
        random = random.replace("-", "")
        return random[0:5]

    @staticmethod
    def create_unique_friend_id_():
        flag = True
        friend_id = "aaaaa"
        while flag:
            try:
                Code.objects.get(friend_id=friend_id)
            except:
                flag = False
                friend_id = Code.generate_frined_key_()
        return friend_id
