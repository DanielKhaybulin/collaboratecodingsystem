from django.urls import re_path, path

from .views import RegistrationAPIView
from .views import LoginAPIView, get_login

urlpatterns = [
    re_path(
        r"^registration/?$", RegistrationAPIView.as_view(), name="user_registration"
    ),
    re_path(r"^login/?$", LoginAPIView.as_view(), name="user_login"),
    path("check_auth/", get_login),
]
